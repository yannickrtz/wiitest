#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
	:QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, wiiAnalyser(new WiiAnalyser)
{
	ui->setupUi(this);

	// We have to register structs that we want to use with QObjects and SLOTs
	qRegisterMetaType<wiiinfo>();
	connect(wiiAnalyser, SIGNAL(sendChord(float)), this, SLOT(receiveChord(float)));
	connect(wiiAnalyser, SIGNAL(sendStrum(float)), this, SLOT(receiveStrum(float)));
	connect(wiiAnalyser, SIGNAL(sendConnect()), this, SLOT(receiveConnect()));
}

MainWindow::~MainWindow()
{
    delete ui;
	delete wiiAnalyser;
}

void MainWindow::receiveChord(float speed)
{
    qDebug() << "Chord " << speed;
}

void MainWindow::receiveStrum(float speed)
{
	qDebug() << "Strum" << speed;
}

void MainWindow::receiveConnect()
{
	ui->label_2->setText("CONNECTETETE");
}