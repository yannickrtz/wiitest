/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QLabel *nunButtonLabel;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *zAccLabel;
    QLabel *yAccLabel;
    QLabel *mainButtonLabel;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(475, 461);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(30, 370, 75, 23));
        nunButtonLabel = new QLabel(centralWidget);
        nunButtonLabel->setObjectName(QStringLiteral("nunButtonLabel"));
        nunButtonLabel->setGeometry(QRect(170, 110, 46, 13));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(200, 170, 46, 13));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(190, 240, 46, 13));
        zAccLabel = new QLabel(centralWidget);
        zAccLabel->setObjectName(QStringLiteral("zAccLabel"));
        zAccLabel->setGeometry(QRect(240, 240, 161, 16));
        yAccLabel = new QLabel(centralWidget);
        yAccLabel->setObjectName(QStringLiteral("yAccLabel"));
        yAccLabel->setGeometry(QRect(250, 170, 111, 16));
        mainButtonLabel = new QLabel(centralWidget);
        mainButtonLabel->setObjectName(QStringLiteral("mainButtonLabel"));
        mainButtonLabel->setGeometry(QRect(290, 110, 46, 13));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 475, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        pushButton->setText(QApplication::translate("MainWindow", "Connect", 0));
        nunButtonLabel->setText(QApplication::translate("MainWindow", "nunButton", 0));
        label_2->setText(QApplication::translate("MainWindow", "Y-Acc", 0));
        label_3->setText(QApplication::translate("MainWindow", "Z-Acc", 0));
        zAccLabel->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        yAccLabel->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        mainButtonLabel->setText(QApplication::translate("MainWindow", "mainButton", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
