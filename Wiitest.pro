#-------------------------------------------------
#
# Project created by QtCreator 2014-08-01T12:45:41
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = Wiitest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        wiiyourself/wiimote.cpp \
        wiimotethread.cpp \
        WiiAnalyser.cpp

HEADERS += mainwindow.h \
        wiiyourself/wiimote.h \
        wiiyourself/wiimote_state.h \
        wiimotethread.h \
        WiiAnalyser.h

FORMS    += mainwindow.ui

include(opencv.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/wiiyourself/lib/ -lWiiYourself!_
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/wiiyourself/lib/ -lWiiYourself!_d
else:unix: LIBS += -L$$PWD/wiiyourself/lib/ -lWiiYourself!_

INCLUDEPATH += $$PWD/wiiyourself
DEPENDPATH += $$PWD/wiiyourself
