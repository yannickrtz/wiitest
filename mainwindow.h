#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/opencv.hpp>
#include "WiiAnalyser.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <qdebug.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
	void receiveStrum(float speed);
    void receiveChord(float speed);
	void receiveConnect();

private:
    Ui::MainWindow *ui;
	WiiAnalyser *wiiAnalyser;
};

#endif // MAINWINDOW_H
